# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number = rand(1..100)
  p "guess a number?"
  input = gets.to_i # don't need to chomp if you're converting to Integer
  count = 1
  p input
  until input == number
    if input > number
      p "too high"
    elsif input < number
      p "too low"
    end
    p "Please Guess Again"
    input = gets.to_i
    count += 1
  end
  p "The correct number was #{number}. It took you #{count} guesses"
end

def file_shuffler
  p "What is the file name?"
  file_name = gets.chomp
  lines = File.readlines("#{file_name}")
  lines.shuffle!
  shuffled_file = File.open("#{file_name}-shuffled.txt", "w")
  lines.each { |line| shuffled_file.puts(line) }
  shuffled_file.close

  p "Your file has been saved as #{file_name}-shuffled.txt"

end

 if  $0 == __FILE__ 
   file_shuffler
 end
